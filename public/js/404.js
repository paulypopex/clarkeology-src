/* global location */
(function (location) {
  const messageMatch = /m\/(\d+)(.*)/.exec(location)
  const gigMatch = /gig\/.*\/(\d+)\.html$/.exec(location)
  const venueMatch = /-(\d+)\//.exec(location)
  const personMatch = /names\/.+\+/.exec(location)
  const phpMatch = /php/.exec(location)
  const replyMatch = /r\/\d+/.exec(location)
  const wikiMatch = /wiki\/(.+)/.exec(location)
  if (messageMatch) {
    if (messageMatch[2]) location.href = '/m/' + messageMatch[1]
    else location.href = 'http://www.clarkeology.com/m/' + messageMatch[1]
  } else if (venueMatch) {
    location.href = '/v/' + venueMatch[1]
  } else if (gigMatch) {
    location.href = '/gig/' + gigMatch[1]
  } else if (personMatch) {
    location.href = location.href.replace(/\+/g, '-')
  } else if (phpMatch || replyMatch) {
    location.href = '/a#' + location.pathname
  } else if (wikiMatch) {
    location.href = '/wiki/#' + wikiMatch[1].replace(/\+/g, '/')
  }
})(location)
