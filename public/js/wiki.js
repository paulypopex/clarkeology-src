/* global location, XMLHttpRequest */
(function (location) {
  const get = function (url, callback, request) {
    request = new XMLHttpRequest() // sorry ie6 etc
    request.open('GET', url, true)
    request.onreadystatechange = function () {
      if (request.readyState == 4 && request.status == 200) { // eslint-disable-line eqeqeq
        callback(request.responseText)
      }
    }
    request.send()
  }
  const hash = location.hash
  if (!hash) return
  const path = hash.substr(1)
  const div = document.getElementsByTagName('div')[0]
  const h2 = '<h2>' + path.replace(/\W/g, ' ') + '</h2>'
  div.innerHTML = h2
  get('/names/wiki/' + path, function (names) {
    div.innerHTML = h2 + wiki
    get('/wiki/' + path, function (wiki) {
      div.innerHTML = h2 + wiki + names
      get('/gig/wiki/' + path, function (wiki) {
        div.innerHTML = h2 + wiki + names
      })
    })
  })
})(location)
